﻿using UnityEngine;
using System.Collections;

public static class MathUtils
{
	public static float LinearMap( float inValue, float inMin, float inMax, float outMin, float outMax )
	{
		return DoLinearMap( inValue, inMin, inMax, outMin, outMax, false );
	}

	public static float LinearMapClamped( float inValue, float inMin, float inMax, float outMin, float outMax )
	{
		return DoLinearMap( inValue, inMin, inMax, outMin, outMax, true );
	}

	private static float DoLinearMap( float inValue, float inMin, float inMax, float outMin, float outMax, bool clamp )
	{
		float inRange = inMax - inMin;
		
		if (inRange == 0.0f)
		{
			return outMin;
		}
		
		float t = (inValue - inMin) / inRange;

		if (clamp)
		{
			t = Mathf.Clamp01( t );
		}
		
		float outValue = outMin + (outMax - outMin) * t;

		return outValue;
	}
	
    public static float ConvertLerpAmountToVariableTime( float fixedTimeLerpAmount, float originalFPS )
    {
        return Mathf.Pow( 1 - fixedTimeLerpAmount, originalFPS );
    }

    public static float ConvertLerpAmountToFixedTime( float variableTimeLerpAmount, float dT )
    {
        return 1.0f - Mathf.Pow( variableTimeLerpAmount, dT );
    }

    public static float ConvertLerpAmountBetweenTimeSteps( float originalLerpAmount, float originalFPS, float newDT )
    {
        return ConvertLerpAmountToFixedTime( ConvertLerpAmountToVariableTime( originalLerpAmount, originalFPS ), newDT );
    }
}
