﻿using UnityEngine;
using System.Collections;

namespace Toast
{
	public class PerformanceTimer
	{
		private float		_timeAtStart;
		private float		_timeAtEnd;
		private string		_name;

		public PerformanceTimer( string name )
		{
			_name = name;
		}

		public void Start()
		{
			_timeAtStart = Time.realtimeSinceStartup;
		}

		public void Stop()
		{
			_timeAtEnd = Time.realtimeSinceStartup;

			var duration = _timeAtEnd - _timeAtStart;

			Debug.Log( _name + " took " + duration.ToString( "0.0000" ) + " seconds." );
		}
	}
}