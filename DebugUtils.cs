﻿//#define USE_KGFDEBUG

using UnityEngine;
using System;
using System.Diagnostics;

namespace Toast
{
	class DebugUtils
	{
		[Conditional("TOAST_DEBUG"), Conditional("UNITY_EDITOR")]
		public static void AssertMsg(bool condition, string messageString)
		{
			if (!condition)
			{
				UnityEngine.Debug.LogError( messageString );
				UnityEngine.Debug.Break();
				//throw new Exception();
			}
		}
		
		[Conditional("TOAST_DEBUG"), Conditional("UNITY_EDITOR")]
		public static void Log(string category, string debugString)
		{
//			UnityEngine.Debug.Log( debugString );
#if USE_KGFDEBUG
			KGFDebug.LogDebug( debugString, category );
#endif
			UnityEngine.Debug.Log( "* " + category + " * " + debugString );
		}
	}
}

