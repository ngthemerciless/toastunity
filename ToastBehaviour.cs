﻿using UnityEngine;
using System.Collections;

public class ToastBehaviour : MonoBehaviour 
{
	public static bool updateLogicInFixedUpdate = false;

	protected virtual void DoLogicUpdate() {}
	protected virtual void DoFixedUpdate() {}

	void FixedUpdate()
	{
		DoFixedUpdate();

		if (updateLogicInFixedUpdate)
		{
			DoLogicUpdate();
		}
	}

	void Update()
	{
		if (!updateLogicInFixedUpdate)
		{
			DoLogicUpdate();
		}
	}
}
