﻿using UnityEngine;
using System.Collections;

public class BinaryEdgeDetector
{
	public bool on;
	public bool prevOn;
	public bool justOn;
	public bool justOff;

	public BinaryEdgeDetector( bool initialOn )
	{
		on = initialOn;
		prevOn = initialOn;
		justOn = false;
		justOff = false;
	}

	public void Update( bool currentOn )
	{
		prevOn = on;
		on = currentOn;

		justOn 	= (!prevOn && on);
		justOff = (prevOn && !on);
	}
}
