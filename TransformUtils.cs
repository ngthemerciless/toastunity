﻿using UnityEngine;
using System.Collections;

public static class TransformUtils
{
	public static string GetPath( this Transform current ) 
	{
		if (current.parent == null)
		{
			return "/" + current.name;
		}

		return current.parent.GetPath() + "/" + current.name;
	}
}
