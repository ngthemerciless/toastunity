﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class PIDController
{
	public float	kP;
	public float 	kI;
	public float	kD;

	private float 	_integral;
	private float 	_prevError;

	public float Tick( float error, float dT )
	{
		_integral += error * dT;

		float derivative = (error - _prevError) / dT; 

		float output = (kP * error) + (kI * _integral) + (kD * derivative);

		_prevError = error; 

		return output;
	}
}
