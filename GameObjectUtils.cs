﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Toast
{
	public static class GameObjectUtils 
	{
		public static void DestroyChildren( this GameObject parentGO, bool immediate = false )
		{
			List<GameObject> childGOs = new List<GameObject>();
			
			foreach (Transform childTransform in parentGO.transform)
			{
				childGOs.Add( childTransform.gameObject );
			}
			
			foreach (GameObject childGO in childGOs)
			{
				if (immediate)
				{
					GameObject.DestroyImmediate( childGO );
				}
				else
				{
					GameObject.Destroy( childGO );
				}
			}
		}

		public static void SetLayerRecursively( this GameObject obj, int layer )
		{
			obj.layer = layer;
 
			foreach (Transform child in obj.transform)
			{
				child.gameObject.SetLayerRecursively( layer );
			}
		}
	}
}
